<Q                         SOFTPARTICLES_ON	   _EMISSION         _COLORCOLOR_ON  
   _FADING_ON     _REQUIRE_UV2�  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ProjectionParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in mediump vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp float in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec2 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD3;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat1;
    vs_COLOR0 = in_COLOR0;
    vs_TEXCOORD1.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD2.xy = in_TEXCOORD0.zw;
    vs_TEXCOORD2.z = in_TEXCOORD1;
    u_xlat2 = u_xlat0.y * hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat0.x = hlslcc_mtx4x4unity_MatrixV[0].z * u_xlat0.x + u_xlat2;
    u_xlat0.x = hlslcc_mtx4x4unity_MatrixV[2].z * u_xlat0.z + u_xlat0.x;
    u_xlat0.x = hlslcc_mtx4x4unity_MatrixV[3].z * u_xlat0.w + u_xlat0.x;
    vs_TEXCOORD3.z = (-u_xlat0.x);
    u_xlat0.x = u_xlat1.y * _ProjectionParams.x;
    u_xlat0.w = u_xlat0.x * 0.5;
    u_xlat0.xz = u_xlat1.xw * vec2(0.5, 0.5);
    vs_TEXCOORD3.w = u_xlat1.w;
    vs_TEXCOORD3.xy = u_xlat0.zz + u_xlat0.xw;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ZBufferParams;
uniform 	mediump vec4 _Color;
uniform 	mediump vec3 _EmissionColor;
uniform 	vec4 _SoftParticleFadeParams;
uniform 	vec4 _CameraFadeParams;
UNITY_LOCATION(0) uniform mediump sampler2D _MainTex;
UNITY_LOCATION(1) uniform highp sampler2D _CameraDepthTexture;
UNITY_LOCATION(2) uniform mediump sampler2D _EmissionMap;
in highp vec4 vs_COLOR0;
in highp vec2 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
in highp vec4 vs_TEXCOORD3;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
mediump vec4 u_xlat16_0;
bool u_xlatb0;
vec3 u_xlat1;
mediump vec4 u_xlat16_1;
mediump vec3 u_xlat16_2;
vec3 u_xlat3;
bvec2 u_xlatb3;
mediump vec4 u_xlat16_4;
vec3 u_xlat5;
mediump vec3 u_xlat16_5;
vec3 u_xlat6;
mediump vec3 u_xlat16_6;
bool u_xlatb7;
mediump vec3 u_xlat16_9;
vec3 u_xlat10;
mediump float u_xlat16_11;
bool u_xlatb14;
mediump float u_xlat16_16;
mediump float u_xlat16_23;
void main()
{
    u_xlat16_0.xyz = texture(_MainTex, vs_TEXCOORD1.xy).xyz;
    u_xlat16_1.xyz = texture(_MainTex, vs_TEXCOORD2.xy).xyz;
    u_xlat1.xyz = (-u_xlat16_0.xyz) + u_xlat16_1.xyz;
    u_xlat0.xyz = vs_TEXCOORD2.zzz * u_xlat1.xyz + u_xlat16_0.xyz;
    u_xlat16_2.xyz = u_xlat0.xyz * _Color.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb14 = !!(u_xlat16_2.y>=u_xlat16_2.z);
#else
    u_xlatb14 = u_xlat16_2.y>=u_xlat16_2.z;
#endif
    u_xlat16_9.x = (u_xlatb14) ? 1.0 : 0.0;
    u_xlat16_23 = u_xlat0.y * _Color.y + (-u_xlat16_2.z);
    u_xlat16_9.x = u_xlat16_9.x * u_xlat16_23 + u_xlat16_2.z;
#ifdef UNITY_ADRENO_ES3
    u_xlatb7 = !!(u_xlat16_2.x>=u_xlat16_9.x);
#else
    u_xlatb7 = u_xlat16_2.x>=u_xlat16_9.x;
#endif
    u_xlat16_2.x = (u_xlatb7) ? 1.0 : 0.0;
    u_xlat16_16 = u_xlat0.x * _Color.x + (-u_xlat16_9.x);
    u_xlat16_2.x = u_xlat16_2.x * u_xlat16_16 + u_xlat16_9.x;
#ifdef UNITY_ADRENO_ES3
    u_xlatb0 = !!(vs_COLOR0.y>=vs_COLOR0.z);
#else
    u_xlatb0 = vs_COLOR0.y>=vs_COLOR0.z;
#endif
    u_xlat16_1.xy = (-vs_COLOR0.zy) + vs_COLOR0.yz;
    u_xlat16_1.z = float(1.0);
    u_xlat16_1.w = float(-1.0);
    u_xlat16_0 = (bool(u_xlatb0)) ? u_xlat16_1 : vec4(0.0, 0.0, 0.0, -0.0);
    u_xlat16_1.xy = u_xlat16_0.xy + vs_COLOR0.zy;
    u_xlat16_1.zw = u_xlat16_0.zw + vec2(-1.0, 0.666666687);
#ifdef UNITY_ADRENO_ES3
    u_xlatb3.x = !!(vs_COLOR0.x>=u_xlat16_1.x);
#else
    u_xlatb3.x = vs_COLOR0.x>=u_xlat16_1.x;
#endif
    u_xlat16_0.xyz = (-u_xlat16_1.xyw);
    u_xlat16_0.w = (-vs_COLOR0.x);
    u_xlat16_4.x = u_xlat16_0.x + vs_COLOR0.x;
    u_xlat16_4.yzw = u_xlat16_0.yzw + u_xlat16_1.yzx;
    u_xlat16_0 = (u_xlatb3.x) ? u_xlat16_4 : vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat16_9.xyz = u_xlat16_0.xyz + u_xlat16_1.xyw;
    u_xlat16_4.x = u_xlat16_0.w + vs_COLOR0.x;
    u_xlat16_11 = min(u_xlat16_9.y, u_xlat16_4.x);
    u_xlat16_11 = u_xlat16_9.x + (-u_xlat16_11);
    u_xlat16_16 = (-u_xlat16_9.y) + u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_11 * 6.0 + 9.99999975e-05;
    u_xlat16_16 = u_xlat16_16 / u_xlat16_4.x;
    u_xlat16_16 = u_xlat16_16 + u_xlat16_9.z;
    u_xlat16_9.x = u_xlat16_9.x + 9.99999975e-05;
    u_xlat16_9.x = u_xlat16_11 / u_xlat16_9.x;
    u_xlat16_4.xyz = abs(vec3(u_xlat16_16)) + vec3(1.0, 0.666666687, 0.333333343);
    u_xlat16_4.xyz = fract(u_xlat16_4.xyz);
    u_xlat16_4.xyz = u_xlat16_4.xyz * vec3(6.0, 6.0, 6.0) + vec3(-3.0, -3.0, -3.0);
    u_xlat16_4.xyz = abs(u_xlat16_4.xyz) + vec3(-1.0, -1.0, -1.0);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_4.xyz = min(max(u_xlat16_4.xyz, 0.0), 1.0);
#else
    u_xlat16_4.xyz = clamp(u_xlat16_4.xyz, 0.0, 1.0);
#endif
    u_xlat16_4.xyz = u_xlat16_4.xyz + vec3(-1.0, -1.0, -1.0);
    u_xlat16_9.xyz = u_xlat16_9.xxx * u_xlat16_4.xyz + vec3(1.0, 1.0, 1.0);
    u_xlatb3.xy = lessThan(vec4(0.0, 0.0, 0.0, 0.0), _SoftParticleFadeParams.xyxx).xy;
    u_xlatb3.x = u_xlatb3.y || u_xlatb3.x;
    if(u_xlatb3.x){
        u_xlat3.xy = vs_TEXCOORD3.xy / vs_TEXCOORD3.ww;
        u_xlat3.x = texture(_CameraDepthTexture, u_xlat3.xy).x;
        u_xlat3.x = _ZBufferParams.z * u_xlat3.x + _ZBufferParams.w;
        u_xlat3.x = float(1.0) / u_xlat3.x;
        u_xlat3.x = u_xlat3.x + (-_SoftParticleFadeParams.x);
        u_xlat3.x = u_xlat3.x + (-vs_TEXCOORD3.z);
        u_xlat3.x = u_xlat3.x * _SoftParticleFadeParams.y;
#ifdef UNITY_ADRENO_ES3
        u_xlat3.x = min(max(u_xlat3.x, 0.0), 1.0);
#else
        u_xlat3.x = clamp(u_xlat3.x, 0.0, 1.0);
#endif
    } else {
        u_xlat3.x = 1.0;
    }
    u_xlat10.x = vs_TEXCOORD3.z + (-_CameraFadeParams.x);
    u_xlat10.x = u_xlat10.x * _CameraFadeParams.y;
#ifdef UNITY_ADRENO_ES3
    u_xlat10.x = min(max(u_xlat10.x, 0.0), 1.0);
#else
    u_xlat10.x = clamp(u_xlat10.x, 0.0, 1.0);
#endif
    u_xlat16_5.xyz = texture(_EmissionMap, vs_TEXCOORD1.xy).xyz;
    u_xlat16_6.xyz = texture(_EmissionMap, vs_TEXCOORD2.xy).xyz;
    u_xlat6.xyz = (-u_xlat16_5.xyz) + u_xlat16_6.xyz;
    u_xlat5.xyz = vs_TEXCOORD2.zzz * u_xlat6.xyz + u_xlat16_5.xyz;
    u_xlat16_4.xyz = u_xlat5.xyz * vec3(_EmissionColor.x, _EmissionColor.y, _EmissionColor.z);
    u_xlat10.xyz = u_xlat10.xxx * u_xlat16_4.xyz;
    u_xlat3.xyz = u_xlat3.xxx * u_xlat10.xyz;
    u_xlat3.xyz = u_xlat16_2.xxx * u_xlat16_9.xyz + u_xlat3.xyz;
    SV_Target0.xyz = u_xlat3.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif
   9                             $GlobalsP         _ZBufferParams                           _Color                          _EmissionColor                           _SoftParticleFadeParams                   0      _CameraFadeParams                     @          $Globals�         _ProjectionParams                            unity_ObjectToWorld                        unity_MatrixV                    P      unity_MatrixVP                   �             _MainTex                  _CameraDepthTexture                 _EmissionMap             