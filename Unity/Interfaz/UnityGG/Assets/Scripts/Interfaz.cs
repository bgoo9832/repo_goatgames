﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Interfaz : MonoBehaviour
{
    public void Salir()
    {
        Application.Quit();
        Debug.Log("Se ha salido del juego");
    }

    public void LoadScene(string escena)
    {
        if (GameManager.Instance != null)
        {
            GameManager.Instance.Reset();
        }
        
        SceneManager.LoadScene(escena);
    }
}