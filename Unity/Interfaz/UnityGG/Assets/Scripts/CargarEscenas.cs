﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CargarEscenas : MonoBehaviour
{
    public void LoadScene(string escena)
    {
        GameManager.Instance.Reset();
        SceneManager.LoadScene(escena);
    }
    public void Exit()
    {
        Application.Quit();
    }
}
