﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotation : MonoBehaviour
{
    // Start is called before the first frame update
    public Rigidbody rb;
    public Joystick joystick;
    

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    
    }

    // Update is called once per frame
    void Update()
    {
        
        Vector3 origen = rb.position;
        if (joystick.Vertical != 0 || joystick.Horizontal != 0)
        {
            Vector2 convertedXY = new Vector2(origen.x + joystick.Vertical*-50,origen.z + joystick.Horizontal*50);
            Vector3 direction = new Vector3(convertedXY.x, 1, convertedXY.y);
            transform.LookAt(direction);
        }
        
        
    }
}
