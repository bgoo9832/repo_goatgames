﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    /* prueba1
    [SerializeField]
    Joystick joystick;
    [SerializeField]
    Transform PlayerSprite;
    */
    float horizontalMove = 0;
    float verticalMove = 0;
    public float runSpeedHorizontal = 3;
    public float runSpeedVertical = 3;
    public float runSpeed = 0;
    public Joystick joystick;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        if ( joystick == null)
        {
            joystick = GameObject.FindGameObjectWithTag("Joystick1").GetComponent<Joystick>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        /* prueba1
        PlayerSprite.position = new Vector3(joystick.Horizontal + transform.position.z,0 , joystick.Vertical + transform.position.x);
        transform.LookAt(new Vector3(PlayerSprite.position.x, 0, PlayerSprite.position.z));
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
        if(joystick.Horizontal > 0 || joystick.Horizontal < 0 || joystick.Vertical > 0 || joystick.Vertical < 0)
        {
            transform.Translate(Vector3.forward * Time.deltaTime*-2);
        }
        */
       verticalMove = joystick.Vertical * runSpeedVertical * -1;
       horizontalMove = joystick.Horizontal * runSpeedHorizontal;
       transform.position+=new Vector3(verticalMove,0,horizontalMove) * Time.deltaTime * runSpeed;
       if(joystick.Vertical !=0)
       {
           anim.SetFloat("nose",1);
       }
       if(joystick.Horizontal !=0)
       {
           anim.SetFloat("nose",1);
       }
       if(joystick.Vertical ==0 && joystick.Horizontal == 0)
       {
           anim.SetFloat("nose",0);
       }
        
    }
   
}
