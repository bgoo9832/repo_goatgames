﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pepazo : MonoBehaviour
{
    [Tooltip("municion que se lanza")]
    public GameObject ammo;
    [Tooltip("Donde se generan la municion")]
    public Transform spawnPosition;
    [Tooltip("velocidad disparo")]
    public float acSpeed = 3f;
    private float timeElapsed = 0f;
    public Image super;

    void Start()
    {
        reset();
    }

    void Update()
    {
        timeElapsed += Time.deltaTime;
        super.fillAmount = timeElapsed / 10;
        if (super.fillAmount >= 1)
        {
            super.fillAmount = 1;
            super.color = new Color(255, 255, 255, 255);

        }

    }
    public void Disparo()
    {

        if (super.fillAmount >= 1)
        {
            GameObject newAmmo;
            newAmmo = Instantiate(ammo, spawnPosition.position, spawnPosition.rotation);
            AmmoController ac;
            ac = newAmmo.GetComponent<AmmoController>();
            ac.speed = acSpeed;
            timeElapsed = 0;
            super.fillAmount = 0;
            super.color = new Color(255, 255, 255, 0.5f);
        }


    }
    public void reset()
    {
        super.fillAmount = 0;
        super.color = new Color(255, 255, 255, 0.5f);
        timeElapsed = 0f;
    }
    


}
