﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camara : MonoBehaviour
{
    public GameObject seguir;
    public Vector3 distancia;
    // Start is called before the first frame update
    void Start()
    {
        distancia = (transform.position-seguir.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = seguir.transform.position + distancia;
    }
}
