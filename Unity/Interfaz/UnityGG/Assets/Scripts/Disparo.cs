﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Disparo : MonoBehaviour
{
    [Tooltip("municion que se lanza")]
    public GameObject ammo;
    [Tooltip("Donde se generan la municion")]
    public Transform spawnPosition;
    [Tooltip("velocidad disparo")]
    public float acSpeed= 3f;
    [Tooltip("municion")]
    public uint shoots = 3;
    [Tooltip("tiempo de recarga")]
    public float nextShootTime = 2f;
    


    //efectuar disparo
    public void shoot()
    {
        Debug.Log("Se ha disparado");
        if (shoots > 0)
        {
            wait();
            shoots--;
            shootStart();
            Invoke("shootStart", 0.1f);
            Invoke("shootStart", 0.2f);
            Invoke("shootStart", 0.3f);
            Invoke("shootStart", 0.4f);
            Invoke("recarga", nextShootTime);

        }


    }
    //esperar tiempo
    public IEnumerator wait()
    {
        yield return new WaitForSeconds(1f);
    }
    //generar carga
    public void shootStart()
    {
        /*
        RaycastHit hit;
        if (Physics.Raycast(spawnPosition.position, spawnPosition.forward, out hit, weaponRange))
        {
            Debug.DrawRay(spawnPosition.position, spawnPosition.forward, Color.green, 10000f);
            Debug.Log("rayucast lanzado" + hit.transform.position.x + "," + hit.transform.position.z);
        }
        */

        GameObject newAmmo;
        newAmmo = Instantiate(ammo, spawnPosition.position, spawnPosition.rotation);
        AmmoController ac;
        ac = newAmmo.GetComponent<AmmoController>();
        ac.speed = acSpeed;
        
    }

    //iniciar recarga de municion
    
    public void recarga()
    {
        if (shoots != 3)
        {
            Debug.Log("recargando...");
            shoots++;

        }

    }
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
