﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pausa : MonoBehaviour
{
    // Start is called before the first frame update
    public static bool GameIsPaused = false;
    public GameObject pauseMenuUI;

    // Update is called once per frame
    public void Update()
    {
        
    }
    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;

    }
    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;

    }
    public void activar()
    {
        if (GameIsPaused == true)
        {
            Resume();
        }
        else
        {
            Pause();
        }
    }
    public void setPause()
    {
        if (GameIsPaused == true)
        {
            GameIsPaused = false;
            Time.timeScale = 1f;
        }

    }
}
