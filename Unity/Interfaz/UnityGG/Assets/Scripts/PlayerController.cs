using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{
    public GameObject detective;
    public Slider vidaUI;
    public float healthd = 300f;
    public GameObject Faraon;
    public float distancia = 2f; 
    private float timeElapsed;
    public Transform spawn;
    public float VidaMax = 300f;
    void Start()
    {
        SetHealth();
        

    }
    void Update()
    {
        timeElapsed += Time.deltaTime;
        if(healthd <= 0)
        {
            GameManager.Instance.restartLevelLose();
            Debug.Log("Muerte personaje");
            restartHealth();
            
        }
        if(Input.GetKeyDown(KeyCode.Space))
        {
            healthd -= 20f;
        }
        vidaUI.value = healthd;
        if(Vector3.Distance(transform.position,Faraon.transform.position) <= distancia)
        {
            if(timeElapsed >= 1f)
            {
                TakeDamage();
                timeElapsed = 0f;
            }
        }


    }
    void ronda()
    {
        SceneManager.LoadScene(3);
    }
    public void SetHealth()
    {
        vidaUI.maxValue = healthd;
        vidaUI.value = healthd;
    }
    public void muerte()
    {
        
    }

    public float damage = 1000f;
    void TakeDamage()
    {
        healthd -= damage;
    }
    public void Revivir()
    {
        Transform transform = GetComponent<Transform>();
        transform.position = spawn.position;
        restartHealth();
    }
    public void restartHealth()
    {
        vidaUI.maxValue = VidaMax;
        vidaUI.value = VidaMax;
        healthd = VidaMax;
    }
    public void Alive()
    {
        Transform transform = GetComponent<Transform>();
        transform.position = spawn.position;

    }

}
