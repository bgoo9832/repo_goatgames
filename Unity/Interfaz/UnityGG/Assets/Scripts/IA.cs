﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class IA : MonoBehaviour
{
    public GameObject Target;
    public NavMeshAgent agent;
    public float healthf = 500f;
    public Slider vidaUI;
    public float enemySpeed = 7f;
    public Transform spawnenemy;
    public float MaxHealth = 500f;


    // Update is called once per frame
    void Update()
    {
        agent.SetDestination(Target.transform.position);
        vidaUI.value = healthf;
        if(healthf <= 0)
        {
            GameManager.Instance.restartLevel();
            Debug.Log("Muerte enemigo");
            SetHealth();

        }
    }
    public void apoyo()
    {
        agent.speed = enemySpeed;

    }
    void OnTriggerEnter(Collider colision)
    {
        if(colision.gameObject.tag == "Bala")
        {
            healthf -= 10f;
        }
        if (colision.gameObject.tag == "Pepazo")
        {
            healthf -= 300f;
        }



    }
    void ronda()
    {
        SceneManager.LoadScene(3);
    }
    public void Reaparecer()
    {
        Transform transform = GetComponent<Transform>();
        transform.position = spawnenemy.position;
        GameManager.Instance.NextBurst();
        SetHealth();


    }
    void SetHealth()
    {
        vidaUI.maxValue = MaxHealth;
        vidaUI.value = MaxHealth;
        healthf = MaxHealth;
    }

}
