﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject pauseMenuUI;
    public uint currentPoints = 0;
    private uint currentBurst = 0;
    uint lives = 3;
    public Text text;
    public GameObject topBar;
    public GameObject middleBar;
    public GameObject bottomBar;
    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("GameManager.Error");
            }
            return _instance;
        }
    }
    void Update()
    {
        setNumBars();
    }
    [System.Serializable]
    public class BurstConfig
    {
        public float enemySpeed = 7f;
        public float enemyDamage = 100f;
        public uint pointperkill = 1;
    }
    public BurstConfig burstTest;
    public BurstConfig[] bursts;
    void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
            /*
            DontDestroyOnLoad(text);
            DontDestroyOnLoad(topBar);
            DontDestroyOnLoad(middleBar);
            DontDestroyOnLoad(bottomBar);
            */

        }
    }
    void OnDestroy()
    {
        if (this == _instance)
        {
            _instance = null;
        }
    }
    public void NextBurst()
    {
        
        text.text = "x" + (currentPoints);
        if (lives == 0)
        {
            //redireccionar a gameOver
            
            currentBurst = 0;
            lives = 3;
            SceneManager.LoadScene(1);

        }
   
        IA ec = GetEnemy();
        PlayerController pc = GetPlayer();
        if(ec == null)
        {
            Debug.LogError("no hay enemy");
            return;
        }
        if (currentBurst == bursts.Length)
        {
            --currentBurst;
        }
        ec.enemySpeed = bursts[currentBurst].enemySpeed;
        ec.apoyo();
        pc.damage = bursts[currentBurst].enemyDamage;
        currentBurst++;
        Debug.Log("nivel: " + currentBurst);
    }
    //ec
    static IA GetEnemy()
    {
        GameObject enemy;
        enemy = GameObject.FindGameObjectWithTag("Enemy");
        if (enemy != null)
        {
            return enemy.GetComponent<IA>();
        }
        else
        {
            return null;
        }

    }
    static PlayerController GetPlayer()
    {
        GameObject player;
        player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            return player.GetComponent<PlayerController>();
        }
        else
        {
            return null;
        }
    }
    
    public void enemigoMuerto()
    {
        currentPoints += bursts[currentBurst-1].pointperkill;
    }
    public void restartLevel()
    {
        IA ec = GetEnemy();
        PlayerController pc = GetPlayer();
        ec.Reaparecer();
        pc.Alive();
        enemigoMuerto();
    }
    public void restartLevelLose()
    {
        IA ec = GetEnemy();
        PlayerController pc = GetPlayer();
        ec.Reaparecer();
        pc.Revivir();
        lives--;
        currentBurst--;
        setNumBars();
    }
    public void setNumBars()
    {
        topBar.SetActive(lives > 0);
        middleBar.SetActive(lives > 1);
        bottomBar.SetActive(lives > 2);
    }
    public void Reset()
    {
        IA ec = GetEnemy();
        PlayerController pc = GetPlayer();
        ec.Reaparecer();
        pc.Revivir();
        lives = 3;
        currentBurst = 0;
        setNumBars();
        pauseMenuUI.SetActive(false);
        /*
        Pepazo peps = GetPepazo();
        peps.reset();
        */


    }
    /*
    static Pepazo GetPepazo()
    {
        GameObject pepo;
        pepo = GameObject.FindGameObjectWithTag("Pepazo");
        if (pepo != null)
        {
            return pepo.GetComponent<Pepazo>();
        }
        else
        {
            return null;
        }

    }
    */

}
