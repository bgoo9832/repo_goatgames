using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Sprites;
using UnityEngine.UI;


public class Brightness : MonoBehaviour
{
    
    public CanvasGroup uiElement;
    public CanvasGroup uiElement2;
    public CanvasGroup uiElement3;
    public void SetAlpha(float transparencia)
    {
        uiElement.alpha = transparencia;
        uiElement2.alpha = transparencia;
        uiElement3.alpha = transparencia;
    }
}
