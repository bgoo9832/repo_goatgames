﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoController : MonoBehaviour
{
    private Vector3 inicial;
    [Tooltip("Velocidad disparo")]
    public float speed = 3f;
    private Transform objetivo;
    public float weaponRange = 0.1f;



    // Start is called before the first frame update
    void Start()
    {
        objetivo = GetComponent<Transform>();
        inicial = objetivo.position;
    }

    // Update is called once per frame
    void Update()
    {
        /*
        Vector3 currentPos = rb.position;
        currentPos.z += speed * Time.deltaTime;
        currentPos.x += speed * Time.deltaTime;
        rb.MovePosition(currentPos);
        */
        objetivo.position += objetivo.forward * speed * Time.deltaTime;
        //Debug.Log(weaponRange);
        //Debug.Log(objetivo.position);
        if (Vector3.Distance(inicial, objetivo.position) > weaponRange)
        {
            Explode();
        }
        
        void OnTriggerEnter(Collider colision)
        {
            if(colision.gameObject.tag == "Obstaculo")
            {
                Explode();
            }
            if(colision.gameObject.tag == "Enemy")
            {
                Explode();
            }
        }
        
        
       

    }
    public void Explode()
    {
        Destroy(this.gameObject);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Obstaculo"))
        {
            Explode();
        }
    }
}
