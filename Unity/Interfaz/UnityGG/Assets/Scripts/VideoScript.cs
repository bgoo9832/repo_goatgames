﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class VideoScript : MonoBehaviour
{   
    public VideoPlayer video;
    void Awake()
    {

        video.Play();
        video.loopPointReached += CheckOver;
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0)){
            SceneManager.LoadScene(1);
        }
    }
    void CheckOver(UnityEngine.Video.VideoPlayer vp)
    {
        SceneManager.LoadScene(1);//the scene that you want to load after the video has ended.
    }
}
